"""
Agent documentation goes here.
"""

__docformat__ = 'reStructuredText'

from cmath import log
import configparser
import logging
import sys
from volttron.platform.agent import utils
from volttron.platform.vip.agent import Agent, Core, RPC

from datetime import timedelta as td, datetime as dt
from volttron.platform.agent.utils import format_timestamp, get_aware_utc_now
from os import listdir
from os.path import isfile, join
import os
import pandas as pd
import pytz
import time
import collections 
from logging.handlers import TimedRotatingFileHandler

_log = logging.getLogger(__name__)
utils.setup_logging()
__version__ = "0.1"


def tester(config_path, **kwargs):
    """
    Parses the Agent configuration and returns an instance of
    the agent created using that configuration.

    :param config_path: Path to a configuration file.
    :type config_path: str
    :returns: Tester
    :rtype: Tester
    """
    try:
        config = utils.load_config(config_path)
    except Exception:
        config = {}

    if not config:
        _log.info("Using Agent defaults for starting configuration.")

   
    return Tester(config, **kwargs)


class Tester(Agent):
    """
    Document agent constructor here.
    """
    def setup_logger(self,logger_name, log_file, level=logging.DEBUG):
        l = logging.getLogger(logger_name)
        formatter = logging.Formatter('%(asctime)s - %(message)s')

        # fileHandler = logging.FileHandler(log_file, mode='w')
        # fileHandler.setFormatter(formatter)
        streamHandler = logging.StreamHandler()
        streamHandler.setFormatter(formatter)

        l.setLevel(level)
        # l.addHandler(RotatingFileHandler(filename=log_file, 
        #    mode='w', 
        #    maxBytes=2120000, #2120000
        #    backupCount=4))
        l.addHandler(TimedRotatingFileHandler(filename=log_file, when='W6', interval=1, backupCount=60, encoding='utf-8', delay=False))
        l.addHandler(streamHandler)
         

    def __init__(self,config, **kwargs):
        super(Tester, self).__init__(**kwargs)
        _log.debug("vip_identity: " + self.core.identity)
        logs_path=config.get('logs_path','/home/volttron/writeback_logs/')
        self.writebackPath=config.get('writeback_path','/home/volttron/configs/raw_data/writeback')
        self.starting_hour = config.get('starting_hour',9)
        self.starting_mint = config.get('starting_mint',00)
        self.ending_hour = config.get('ending_hour',22)
        self.ending_mint = config.get('ending_mint',00)
        self.devices_path = config.get('devices_path','/KD/GIGA/') # for giga "/KD/GIGA/", for local  "my_campus/my_building/"
        self.temp_sp_name = config.get('temp_sp_name','RTemp_Sp')
        self.vlv_cmd_name = config.get('vlv_cmd_name','Man_Valve_Cmd')
        if not os.path.exists(logs_path):
            os.makedirs(logs_path)
        self.setup_logger('writeback_logger',logs_path+'writeback.log',logging.DEBUG)
        self.writebackLogger=logging.getLogger('writeback_logger')
        # Set a default configuration to ensure that self.configure is called immediately to setup
        # the agent.
        self.default_config = {"setting1": "setting1",
                               "setting2": "setting2"}

        self.vip.config.set_default("config", self.default_config)
        # Hook self.configure up to changes to the configuration file "config".
        self.vip.config.subscribe(self.configure, actions=["NEW", "UPDATE"], pattern="config")

    def configure(self, config_name, action, contents):
        """
        Called after the Agent has connected to the message bus. If a configuration exists at startup
        this will be called before onstart.

        Is called every time the configuration in the store changes.
        """
        config = self.default_config.copy()
        config.update(contents)

        _log.debug("Configuring Agent")
        self.setting2="/hello/topic"
        
        self._create_subscriptions(self.setting2)

    def _create_subscriptions(self, topic):
        """
        Unsubscribe from all pub/sub topics and create a subscription to a topic in the configuration which triggers
        the _handle_publish callback
        """
        self.vip.pubsub.unsubscribe("pubsub", None, None)

        self.vip.pubsub.subscribe(peer='pubsub',
                                  prefix="devices/actuators/",
                                  callback=self._handle_publish)

    def _handle_publish(self, peer, sender, bus, topic, headers, message):
        """
        Callback triggered by the subscription setup using the topic from the agent's config file
        """
        
        if "error" in str(topic):
            _log.info("################## WRITE-BACK ERROR ALERT ################")
            _log.info("########### ERROR in writting value '{}' on Device '{}' ###########".format(str(topic).split('/')[-1], str(topic).split('/')[-2]))
            _log.info("########### ERROR MESSAGE :'{}' ###########".format(message))
            
            self.writebackLogger.debug("################## WRITE-BACK ERROR ALERT ################")
            self.writebackLogger.debug("########### ERROR in writting value '{}' on Device '{}' ###########".format(str(topic).split('/')[-1], str(topic).split('/')[-2]))
            self.writebackLogger.debug("########### ERROR MESSAGE :'{}' ###########".format(message))
        else:
            _log.info("#########  TOPIC :  {} MESSAGE : {} #########".format(topic, message))
            self.writebackLogger.debug("#########  TOPIC :  {} MESSAGE : {} #########".format(topic, message))



    @Core.receiver("onstart")
    def onstart(self, sender, **kwargs):
        """
        This is method is called once the Agent has successfully connected to the platform.
        This is a good place to setup subscriptions if they are not dynamic or
        do any other startup activities that require a connection to the message bus.
        Called after any configurations methods that are called at startup.

        Usually not needed if using the configuration store.
        """
        
        self.core.periodic(600, self.actuate_point)
        



    # Function to get values for the write back.
    def get_values(self, Path, AHU_name):
        try :
            if not (os.path.isdir(Path)):
                self.writebackLogger.debug("The folder doesn't exist. Returning the default values for device'{AHU_name}'")
                return 23,70

            if not (os.path.isfile(Path+"/"+AHU_name)):
                self.writebackLogger.debug("The file doesn't exist. Returning the default values for device'{AHU_name}'")
                return 23,70

            if (os.path.getsize(Path+"/"+AHU_name) == 0):
                self.writebackLogger.debug("The file is empty. Returning the default values for device'{AHU_name}'")
                return 23,70

            df = pd.read_csv(Path+"/"+AHU_name)
            if df.empty:
                self.writebackLogger.debug("Data doesn't exist in the file. Returning the default values for device'{AHU_name}'")
                return 23,70

            tz = pytz.timezone('Asia/Karachi')
            Pakistan_now = dt.now(tz)
            delta = td(minutes=10)
            ref = dt.min.replace(tzinfo=Pakistan_now.tzinfo)
            modified_time = ref + round((Pakistan_now - ref)//delta)*delta
            modified_time = modified_time.strftime("%d/%m/%Y %H:%M")
            
            # changing format of timestamp to compare
            df["Timestamp"] = df["Timestamp"].apply(lambda x: dt.strptime(x[:-9], '%Y-%m-%d %H:%M').strftime("%d/%m/%Y %H:%M"))
            
            if not(modified_time in df.values):
                self.writebackLogger.debug(f"The required time slot {modified_time}  is not found in file for device'{AHU_name}'. Returning the default values.")
                return 23,70
 
            required_row = df.loc[df['Timestamp'] == modified_time]
            Temp_sp =  required_row["Temp_sp"].iloc[0]
            vlv_cmd = required_row["Valve_CMD"].iloc[0]

            self.writebackLogger.debug(f"The required values found for time slot : '{modified_time}' for device'{AHU_name}'. Returning values as RTemp_sp : '{Temp_sp}' ,  vlv_cmd: '{vlv_cmd}'")
            return Temp_sp,vlv_cmd
        except Exception as e:
            self.writebackLogger.debug(f"Returning the default values for device'{AHU_name}'. An Exception occurred while getting values from file. Exception is: {e} ")
            return 23,70


    def getConfigs(self,device_name):
        try:
            config = configparser.ConfigParser()
            config.read('/home/volttron/configs/writeback_configs.conf')
            try:
                return [str(config.get(str(device_name),"temp_sp_name")), str(config.get(str(device_name),"vlv_cmd_name")), str(config.get(str(device_name),"writeback"))]
            except Exception as e:
                print(e)
                self.writebackLogger.debug("###### ERROR in reading configs of {} ########".format(device_name))
                return [str(config.get('default','temp_sp_name')), str(config.get('default','vlv_cmd_name')), str(config.get('default','writeback'))]
        except:
            self.writebackLogger.debug("###### ERROR in opeing writeback_configs.conf  ########")
        


    def actuate_point(self):
        """
        Request that the Actuator set points on all the devices
        """
        
        # initializing variables
        folder_path = self.writebackPath
        starting_hour = self.starting_hour
        starting_mint = self.starting_mint
        ending_hour = self.ending_hour
        ending_mint = self.ending_mint


        # Create a start and end timestep to serve as the times we reserve to communicate with the Device
        _now = get_aware_utc_now()
        str_now = format_timestamp(_now)
        _end = _now + td(seconds=10)
        str_end = format_timestamp(_end)

        tz = pytz.timezone('Asia/Karachi')
        Pakistan_now = dt.now(tz)
        Pakistan_now_hour = Pakistan_now.hour
        Pakistan_now_mint = Pakistan_now.minute
        if (ending_mint == 0):
            ending_hour = ending_hour -1
            ending_mint = 59
            print("################# hour : ",ending_hour,Pakistan_now_hour)
            print("################# mint : ",ending_mint,Pakistan_now_mint)
        if (Pakistan_now_hour >= starting_hour and Pakistan_now_hour <= ending_hour) and (Pakistan_now_mint >= starting_mint and Pakistan_now_mint <= ending_mint):

            list_is = [f[:-4] for f in listdir(folder_path) if isfile(join(folder_path, f)) and str(f[-4:])==".csv"]
            
            groups_config = configparser.ConfigParser()
            groups_config.read('/home/volttron/configs/writeback_groupings.config')
            groups_dict = collections.defaultdict(list) 
            for list_element in list_is:
                try:
                    groups_dict[int(groups_config.get(str(list_element),"group_no"))].append(list_element)
                except:
                    groups_dict[int(groups_config.get(str("default"),"group_no"))].append(list_element)
            
            ordered_dict = collections.OrderedDict(sorted(groups_dict.items()))
            _log.debug("####### GROUP Dict '{}' ########".format(groups_dict))
            
            for key,group_list in ordered_dict.items():
                
                
                _log.debug("#######  GROUP LIST '{}'  ########".format( group_list))
                for list_element in group_list:
                    _log.debug("####### Writting Device '{}', group '{}'  at {}  ########".format(list_element,(int(key)+1),Pakistan_now))
                    self.writebackLogger.debug("####### Writting Device '{}', group '{}' at {} ########".format(list_element, (int(key)+1),Pakistan_now))
                    
                    device_config_list = self.getConfigs(list_element)
                    if (device_config_list[2].lower() == 'true'):
                        self.temp_sp_name = device_config_list[0]
                        self.vlv_cmd_name = device_config_list[1]

                        _log.debug("###### reading values from file {}.csv ########".format(list_element))
                        self.writebackLogger.debug("###### reading values from file {}.csv ########".format(list_element))
                        temp_sp,vlv_cmd = self.get_values(folder_path, str(list_element)+".csv")
                        vlv_cmd = int(vlv_cmd)
                        temp_sp = int(temp_sp)


                        if vlv_cmd == 0:
                            vlv_cmd = 1

                        _log.debug("####### scheduling time slot for {} ########".format(list_element))
                        self.writebackLogger.debug("####### scheduling time slot for {} ########".format(list_element))
                        schedule_request = [[str(self.devices_path)+str(list_element), str_now, str_end]]
                        header = {
                        'type': 'NEW_SCHEDULE',
                        'requesterID': 'TestAgent', # The name of the requesting agent.
                        'taskID': "my_task_"+str(list_element), # unique (to all tasks) ID for scheduled task.
                        'priority': 'HIGH', #('HIGH, 'LOW', 'LOW_PREEMPT').
                        }
                        self.vip.pubsub.publish('pubsub', "devices/actuators/schedule/request", message=schedule_request, headers=header)

                        _log.debug("####### publishing/setting both points of {} ########".format(list_element))
                        self.writebackLogger.debug("####### publishing/setting both points of {} ########".format(list_element))
                        self.vip.pubsub.publish('pubsub', "devices/actuators/set/"+str(self.devices_path)+str(list_element)+"/"+str(self.temp_sp_name), message=temp_sp, headers={'requesterID': 'TestAgent'})
                        self.vip.pubsub.publish('pubsub', "devices/actuators/set/"+str(self.devices_path)+str(list_element)+"/"+str(self.vlv_cmd_name), message=vlv_cmd, headers={'requesterID': 'TestAgent'})
                
                    else:
                        _log.debug("####### writeback for device '{}' is set FALSE  ########".format(list_element))
                        self.writebackLogger.debug("####### writeback for device '{}' is set FALSE ########".format(list_element))
                time.sleep(3)
                    
               
                
        else:
            _log.debug("#######  current time is not in write-back time duration  ########")
            self.writebackLogger.debug("#######  current time is not in write-back time duration  ########")
            


    @Core.receiver("onstop")
    def onstop(self, sender, **kwargs):
        """
        This method is called when the Agent is about to shutdown, but before it disconnects from
        the message bus.
        """
        pass

    # @RPC.export
    # def rpc_method(self, arg1, arg2, kwarg1=None, kwarg2=None):
    #     """
    #     RPC method

    #     May be called from another agent via self.core.rpc.call
    #     """
    #     return self.setting1 + arg1 - arg2


def main():
    """Main method called to start the agent."""
    utils.vip_main(tester, 
                   version=__version__)


if __name__ == '__main__':
    # Entry point for script
    try:
        sys.exit(main())
    except KeyboardInterrupt:
        pass

